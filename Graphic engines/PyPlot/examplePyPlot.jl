#Install
#Pkg.add("PyPlot")
#using PyPlot
#x = linspace(0,2*pi,1000); y = sin.(3*x + 4*cos.(2*x))
#plot(x, y, color="red", linewidth=2.0, linestyle="--")

#example 2
using PyPlot
x = -2pi:0.1:2pi;
plot(x, sin(x.^2)./x);
