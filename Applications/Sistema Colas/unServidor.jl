using DataFrames
using Gadfly
using RDatasets

#clientes
nroClientes = 10
println("Numeros de Clientes: ", nroClientes)

#numero Aleatorios para llegadas
aletoriosUno = rand(nroClientes)

tEntreLlegada = []
momentosLlegada = []
tiempoInicio = []
tiempoEspera = []

#aleatorio para servicio
aletoriosDos = rand(nroClientes)

tiempoServicio = []
tiempoTerminaServicio = []

#tiempo entre llegadas
function tLlegadas(nroAleUno,cLlegadas)
    tEntreLlegadas::Real = (-((log(1-nroAleUno)/cLlegadas)))*60
    return tEntreLlegadas
end
#tiempo de servico
function tServicio(nroAleDos,cServicio)
    tEntreServicio::Real = (-((log(1-nroAleDos)/cServicio)))*60
    return tEntreServicio
end

#iteracion
x = 1
while x <= nroClientes
    tLleg = tLlegadas(aletoriosUno[x],llegadas)
    tServ = tServicio(aletoriosDos[x],capacidadServicio)
    if x == 1
        append!(tEntreLlegada,round(tLleg,2))
        append!(momentosLlegada,round(tEntreLlegada[x],2))
        append!(tiempoInicio,round(momentosLlegada[x],2))
        append!(tiempoEspera,round(tiempoInicio[x]-momentosLlegada[x],2))
        append!(tiempoServicio,round(tServ,2))
        append!(tiempoTerminaServicio,round(tiempoInicio[x]+tiempoServicio[x],2))
    else
        append!(tEntreLlegada,round(tLleg,2))
        append!(momentosLlegada,round(momentosLlegada[x-1]+tEntreLlegada[x],2))
        append!(tiempoInicio,round(max(momentosLlegada[x],tiempoTerminaServicio[x-1]),2))
        append!(tiempoEspera,round(tiempoInicio[x]-momentosLlegada[x],2))
        append!(tiempoServicio,round(tServ,2))
        append!(tiempoTerminaServicio,round(tiempoInicio[x]+tiempoServicio[x],2))
    end
    x+=1
end
#println(aletoriosUno)
#println(tEntreLlegada)
#println(momentosLlegada)
#println(tiempoInicio)
#println(tiempoEspera)
#println(aletoriosDos)
#println(tiempoServicio)
#println(tiempoTerminaServicio)
#Redendeo usar-->round(aletoriosUno[c],4)

#Mostrar en un tabla
DataFrame(AleatorioUno = aletoriosUno, TiempoLlegadas = tEntreLlegada, 
        MomentoLlegada = momentosLlegada, TiempoInicio = tiempoInicio,
        TiempoEspera = tiempoEspera, AleatorioDos = aletoriosDos,
        TiempoServicio = tiempoServicio, TTerminaServico = tiempoTerminaServicio )

#plot(x = tiempoInicio, y = tiempoServicio,Geom.point, Geom.line)
#plot(x = tiempoServicio, y = tiempoTerminaServicio,Geom.histogram)
