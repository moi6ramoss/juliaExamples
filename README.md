# JULIA EJEMPLOS

### Lenguajes de programación
+ julia 
+ Python 2.x

### Instalacion IJulia
+ https://github.com/JuliaLang/IJulia.jl
+ http://jupyter.readthedocs.io/en/latest/install.html
+ https://www.anaconda.com/download/
+ https://www.supositorio.com/rcalc/rcalclite_esp.htm
+ https://learnxinyminutes.com/docs/es-es/julia-es/

### INTALAR JUPYTER POR CMD
+ Se debe tener instalado Python 2.7 o superior
```shell
pip3 install

upgrade pip

pip3 install jupyter
```


### Consola Julia
Agregar en metadatos
```shell
Pkg.add("IJulia")
```
Para usar y desplejar
```shell
using IJulia
notebook()
```

### Otra forma desplegar CMD
```shell
jupyter notebook
```

### Julia Grafit instalar
```shell
Pkg.add("TextPlots")
```

------------------------------
- Ayuda Julia
-----------------------------


### Ayuda de Sistema de colas

+ http://ingunilibre.blogspot.com/p/teoria-de-colas.html

+ https://www.youtube.com/watch?v=dqEGXm9eh8k

### Dependecia agregadas
Instalar
```shel
Pkg.add("DataFrames")
Pkg.add("Gadfly")
```
Urls de ayuda
+ https://juliadata.github.io/DataFrames.jl/stable/index.html
+ http://gadflyjl.org/stable/tutorial.html
