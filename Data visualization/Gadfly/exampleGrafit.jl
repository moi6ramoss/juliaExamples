using Gadfly
t=collect(linspace(0,20,100))
function exemplo(x::Array{Float64})
    sin(x).* x
end

plot(x=t,y=exemplo(t))


#ERROR: LoadError: Rmath not properly installed. Please run Pkg.build("Rmath") and restart julia